//
//  CellData.swift
//  EcoApp
//
//  Created by naguilar12 on 11/4/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

struct CellData {
    let customImage: UIImage?
    let customText: String?
    let beaconUuid: String?
}
