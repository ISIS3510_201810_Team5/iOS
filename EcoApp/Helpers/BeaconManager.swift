//
//  BeaconManager.swift
//  EcoApp
//
//  Created by naguilar12 on 10/5/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import Foundation
import Alamofire

class BeaconManager {
    
    func calculateCurrentPosition(beaconsUuids: [String], completionHandler:@escaping (String) -> Void){
        
        let url = "https://us-central1-ecoserver-278bf.cloudfunctions.net/calculateCurrentPosition"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        print(supermarket)
        print(beaconsUuids)
        
        let parameters: Parameters = [
            "supermarket_id": supermarket,
            "nearby_beacons": beaconsUuids
        ]
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
        
        Alamofire.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .failure(let error):
                    print(error)
                    
                    if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                        print(responseString)
                    }
                case .success(let responseObject):
                    print("Response Object")
                    print(responseObject)
                    let json = responseObject as! NSDictionary
                    if(json["uuid"] != nil){
                        completionHandler(json["uuid"]! as! String)
                    }else{
                        completionHandler(POSITION_NOT_FOUND)
                    }
                }
        }
        
    }
    
    func getRoutingTable(origin: String, destination: String, completionHandler:@escaping (NSArray) -> Void){
        
        let url = "https://us-central1-test-c4c72.cloudfunctions.net/routingTable"
        let parameters: Parameters = [
            "startPoint": origin,
            "endPoint": destination
        ]
        
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.queryString)
            .responseJSON
            {response in
                switch response.result
                {
                case .success(let responseObject):
                    let jsonData = responseObject as! NSArray
                    //let json = jsonData[0] as! NSDictionary
                    completionHandler(jsonData)
                case .failure(let myError):
                    print(myError)
                }
        }
    }
    
    func getMultipleRoutingTable(origin: String, sections: [String], supermarket: String, completionHandler:@escaping (NSArray) -> Void){
        
        let url = "https://us-central1-ecoserver-278bf.cloudfunctions.net/multipleRoutingTable"
        let parameters: Parameters = [
            "supermarket_id": supermarket,
            "origin": origin,
            "sections": sections
        ]
        
        print(parameters)
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
        
        Alamofire.request(request)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .failure(let error):
                    print(error)
                    
                    if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                        print(responseString)
                    }
                case .success(let responseObject):
                    print(responseObject)
	                    let json = responseObject as! NSArray
                    completionHandler(json)
                }
        }
        
    }
    
    
}

