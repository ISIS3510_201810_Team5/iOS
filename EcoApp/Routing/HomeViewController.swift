//
//  ViewController.swift
//  EcoApp
//
//  Created by naguilar12 on 10/2/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, OEEventsObserverDelegate {
    
    var prints = true
    var slt = Slt()
    var openEarsEventsObserver = OEEventsObserver()
    var fliteController = OEFliteController()
    
    let speechSynthesizer = AVSpeechSynthesizer()
    var startupFailedDueToLackOfPermissions = Bool()
    var restartAttemptsDueToPermissionRequests = Int()
    var pathToFirstDynamicallyGeneratedLanguageModel: String!
    var pathToFirstDynamicallyGeneratedDictionary: String!
    var shouldContinueRecognition = true

    var timer: Timer!
    
    @IBOutlet weak var mainButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareContent()
        
        setGradient(viewController: self)
        customizeMainButton(button: mainButton)
        
        self.openEarsEventsObserver.delegate = self
        startTextToSpeech()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        print("volvio a vista, se resume reconocimiento")
        shouldContinueRecognition = true
        //startTextToSpeech()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    @IBAction func listCategories(_ sender: UIButton) {
        if CheckInternet.Connection() || CacheHelper.categoriesOnCache(){
            performSegue(withIdentifier: "listCategories", sender: self)
        }
            
        else if !CacheHelper.categoriesOnCache(){
            let alrt = Alert(Title: "Internet connection", Message: "Internet is needed to retrieve supermarket categories")
            present(alrt, animated: true, completion: nil)
        }
        
    }
    
    func startTextToSpeech() {
        
        print("Comienza a reconcer voz esta vista")
        
        let languageModelGenerator = OELanguageModelGenerator()
        
        let firstLanguageArray = ["echo",
                                  "be",
                                  "go",
                                  "my",
                                  "eyes",
                                  "search",
                                  "category",
                                  "need",
                                  "I",
                                  "home",
                                  "assistance",
                                  "see",
                                  "list",
                                  "my"]
        
        let firstVocabulary = "FirstVocabulary"
        
        let firstLanguageModelGenerationError: Error! = languageModelGenerator.generateLanguageModel(from: firstLanguageArray, withFilesNamed: firstVocabulary, forAcousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish")) // Change "AcousticModelEnglish" to "AcousticModelSpanish" in order to create a language model for Spanish recognition instead of English.
        
        if(firstLanguageModelGenerationError != nil) {
            if(prints)
            {
                print("Error while creating initial language model: \(firstLanguageModelGenerationError!)")
                
            }
        } else {
            self.pathToFirstDynamicallyGeneratedLanguageModel = languageModelGenerator.pathToSuccessfullyGeneratedLanguageModel(withRequestedName: firstVocabulary) // these are convenience methods you can use to reference the file location of a language model that is known to have been created successfully.
            self.pathToFirstDynamicallyGeneratedDictionary = languageModelGenerator.pathToSuccessfullyGeneratedDictionary(withRequestedName: firstVocabulary)
            do {
                try OEPocketsphinxController.sharedInstance().setActive(true) // Setting the shared OEPocketsphinxController active is necessary before any of its properties are accessed.
            }
            catch {
                if(prints)
                {
                    print("Error: it wasn't possible to set the shared instance to active: \"\(error)\"")
                }
            }
            if(!OEPocketsphinxController.sharedInstance().isListening) {
                OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: self.pathToFirstDynamicallyGeneratedLanguageModel, dictionaryAtPath: self.pathToFirstDynamicallyGeneratedDictionary, acousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"), languageModelIsJSGF: false)
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleHypothesis(_ hypothesis: String!){
        
        if(shouldContinueRecognition){
            let wordsArr = hypothesis.components(separatedBy: " ")
            
            var containsEco = false
            var searchCommand = false
            var assistanceCommand = false
            var beMyEyesCommand = false
            var goHomeCommand = false
            var listCommand = false
            
            
            for word in wordsArr {
                if word == "echo" {
                    containsEco = true
                }
                else if word == "assistance" {
                    assistanceCommand = true
                }
                else if word == "search" {
                    searchCommand = true
                }
                else if word == "category" {
                    searchCommand = true
                }
                else if word == "eyes" {
                    beMyEyesCommand = true
                }
                else if word == "home" {
                    goHomeCommand = true
                }
                else if word == "list" {
                    listCommand = true
                }
                else if word == "my" {
                    listCommand = true
                }
            }
            
            if containsEco && searchCommand {
                let say = "Here is the search list"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
                
                if(false && OEPocketsphinxController.sharedInstance().isListening){
                    let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
                    if(stopListeningError != nil) {
                        if(prints)
                        {
                            print("Error while stopping listening in pocketsphinxFailedNoMicPermissions: \(stopListeningError!)")
                        }
                    }
                }
                // push Categories VC
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CategoryVC") as! CategoriesViewController
                navigationController?.pushViewController(vc, animated: false)
            }
            else if containsEco && assistanceCommand {
                
                //self.fliteController.say(_:"Calling for assistance", with:self.slt)
                let say = "Here you can call for assistance"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
                
                if(false && OEPocketsphinxController.sharedInstance().isListening){
                    let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
                    if(stopListeningError != nil) {
                        if(prints)
                        {
                            print("Error while stopping listening in pocketsphinxFailedNoMicPermissions: \(stopListeningError!)")
                        }
                    }
                }
                // push Assistance VC
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "AssistanceVC") as! AssistanceViewController
                navigationController?.pushViewController(vc, animated: false)
                
            }
            else if containsEco && beMyEyesCommand {
                //self.fliteController.say(_:"Be my eyes activated", with:self.slt)
                let say = "Be my eyes activated"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
                
                if(false && OEPocketsphinxController.sharedInstance().isListening){
                    let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
                    if(stopListeningError != nil) {
                        if(prints)
                        {
                            print("Error while stopping listening in pocketsphinxFailedNoMicPermissions: \(stopListeningError!)")
                        }
                    }
                }
                // push Camera VC
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CamaraVC") as! CameraViewController
                navigationController?.pushViewController(vc, animated: false)
            }
            else if containsEco && goHomeCommand {
                //self.fliteController.say(_:"Going to home screen", with:self.slt)
                let say = "Back to the home screen"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
                
                if(false && OEPocketsphinxController.sharedInstance().isListening){
                    let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
                    if(stopListeningError != nil) {
                        if(prints)
                        {
                            print("Error while stopping listening in pocketsphinxFailedNoMicPermissions: \(stopListeningError!)")
                        }
                    }
                }
                // push Camera VC
                shouldContinueRecognition = false
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
                navigationController?.pushViewController(vc, animated: false)
            }
            else if containsEco && listCommand {
                
                //self.fliteController.say(_:"Here is your list", with:self.slt)
                let say = "Here are your lists"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
                
                if(false && OEPocketsphinxController.sharedInstance().isListening){
                    let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
                    if(stopListeningError != nil) {
                        if(prints)
                        {
                            print("Error while stopping listening in pocketsphinxFailedNoMicPermissions: \(stopListeningError!)")
                        }
                    }
                }
                // push Assistance VC
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ListsVC") as! ListsViewController
                navigationController?.pushViewController(vc, animated: false)
                
            }
            
        }
    }
    
    
    // OE Methods
    
    func pocketsphinxDidReceiveHypothesis(_ hypothesis: String!, recognitionScore: String!, utteranceID: String!) { // Something was heard
        if(prints)
        {
            print("Local callback: The received hypothesis is \(hypothesis!) with a score of \(recognitionScore!) and an ID of \(utteranceID!)")
        }
        //self.fliteController.say(_:"You said \(hypothesis!)", with:self.slt)
        handleHypothesis(hypothesis)
        
    }
    
    // An optional delegate method of OEEventsObserver which informs that the Pocketsphinx recognition loop has entered its actual loop.
    // This might be useful in debugging a conflict between another sound class and Pocketsphinx.
    func pocketsphinxRecognitionLoopDidStart() {
        if(prints)
        {
            print("Local callback: Pocketsphinx started.") // Log it.
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx is now listening for speech.
    func pocketsphinxDidStartListening() {
        if(prints)
        {
            print("Local callback: Pocketsphinx is now listening.") // Log it.
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx detected speech and is starting to process it.
    func pocketsphinxDidDetectSpeech() {
        if(prints)
        {
            print("Local callback: Pocketsphinx has detected speech.") // Log it.
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx detected a second of silence, indicating the end of an utterance.
    func pocketsphinxDidDetectFinishedSpeech() {
        if(prints)
        {
            print("Local callback: Pocketsphinx has detected a second of silence, concluding an utterance.") // Log it.
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx has exited its recognition loop, most
    // likely in response to the OEPocketsphinxController being told to stop listening via the stopListening method.
    func pocketsphinxDidStopListening() {
        if(prints)
        {
            print("Local callback: Pocketsphinx has stopped listening.") // Log it.
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx is still in its listening loop but it is not
    // Going to react to speech until listening is resumed.  This can happen as a result of Flite speech being
    // in progress on an audio route that doesn't support simultaneous Flite speech and Pocketsphinx recognition,
    // or as a result of the OEPocketsphinxController being told to suspend recognition via the suspendRecognition method.
    func pocketsphinxDidSuspendRecognition() {
        if(prints)
        {
            print("Local callback: Pocketsphinx has suspended recognition.") // Log it.
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx is still in its listening loop and after recognition
    // having been suspended it is now resuming.  This can happen as a result of Flite speech completing
    // on an audio route that doesn't support simultaneous Flite speech and Pocketsphinx recognition,
    // or as a result of the OEPocketsphinxController being told to resume recognition via the resumeRecognition method.
    func pocketsphinxDidResumeRecognition() {
        if(prints)
        {
            print("Local callback: Pocketsphinx has resumed recognition.") // Log it.
        }
    }
    
    // An optional delegate method which informs that Pocketsphinx switched over to a new language model at the given URL in the course of
    // recognition. This does not imply that it is a valid file or that recognition will be successful using the file.
    func pocketsphinxDidChangeLanguageModel(toFile newLanguageModelPathAsString: String!, andDictionary newDictionaryPathAsString: String!) {
        if(prints)
        {
            print("Local callback: Pocketsphinx is now using the following language model: \n\(newLanguageModelPathAsString!) and the following dictionary: \(newDictionaryPathAsString!)")
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Flite is speaking, most likely to be useful if debugging a
    // complex interaction between sound classes. You don't have to do anything yourself in order to prevent Pocketsphinx from listening to Flite talk and trying to recognize the speech.
    func fliteDidStartSpeaking() {
        if(prints)
        {
            print("Local callback: Flite has started speaking") // Log it.
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that Flite is finished speaking, most likely to be useful if debugging a
    // complex interaction between sound classes.
    func fliteDidFinishSpeaking() {
        if(prints)
        {
            print("Local callback: Flite has finished speaking") // Log it.
        }
    }
    
    func pocketSphinxContinuousSetupDidFail(withReason reasonForFailure: String!) { // This can let you know that something went wrong with the recognition loop startup. Turn on [OELogging startOpenEarsLogging] to learn why.
        if(prints)
        {
            print("Local callback: Setting up the continuous recognition loop has failed for the reason \(reasonForFailure!), please turn on OELogging.startOpenEarsLogging() to learn more.") // Log it.
        }
    }
    
    func pocketSphinxContinuousTeardownDidFail(withReason reasonForFailure: String!) { // This can let you know that something went wrong with the recognition loop startup. Turn on OELogging.startOpenEarsLogging() to learn why.
        if(prints)
        {
            print("Local callback: Tearing down the continuous recognition loop has failed for the reason \(reasonForFailure!)") // Log it.
        }
    }
    
    /** Pocketsphinx couldn't start because it has no mic permissions (will only be returned on iOS7 or later).*/
    func pocketsphinxFailedNoMicPermissions() {
        if(prints)
        {
            print("Local callback: The user has never set mic permissions or denied permission to this app's mic, so listening will not start.")
        }
    }
    
    /** The user prompt to get mic permissions, or a check of the mic permissions, has completed with a true or a false result  (will only be returned on iOS7 or later).*/
    
    func micPermissionCheckCompleted(withResult: Bool) {
        if(prints)
        {
            print("Local callback: mic check completed.")
        }
    }
    
    func prepareContent(){
        if(CheckInternet.Connection()){
            DispatchQueue.global(qos: .background).async {
                CacheHelper.getCategories(){response in
                }
            }
            
        }
    }
    
    
}
