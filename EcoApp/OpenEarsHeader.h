//
//  Header.h
//  EcoApp
//
//  Created by Alejandro Echeverri on 10/7/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

#ifndef OpenEarsHeader_h
#define OpenEarsHeader_h

#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEFliteController.h>
#import <Slt/Slt.h> // Only needed if using Flite speech
#import <OpenEars/OEEventsObserver.h>
#import <OpenEars/OELogging.h>
#import <OpenEars/OEAcousticModel.h>

#endif /* OpenEarsHeader_h */
