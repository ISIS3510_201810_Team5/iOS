//
//  Constants.swift
//  EcoApp
//
//  Created by naguilar12 on 11/4/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

let FINDING_POSITION = "FINDING_POSITION"
let ON_ROUTE = "ON_ROUTE"
let POSITION_NOT_FOUND = "POSITION_NOT_FOUND"
