//
//  AssistanceViewController.swift
//  EcoApp
//
//  Created by Alejandro Echeverri on 10/7/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseFirestore

class AssistanceViewController: UIViewController, CLLocationManagerDelegate {
    
    // Boton principal para pedir asistencia
    @IBOutlet weak var mainbutton: UIButton!
    // Boton para cancelar la asistencia
    @IBOutlet weak var Cancel: UIButton!
    // Texto que se muestra al usuario
    @IBOutlet weak var textShown: UILabel!
    
    
    // Estado actual de la llamada de asistencia
    var status: String = FINDING_POSITION
    
    var que_paso_amiguito: Bool = true
    var asistenciaLlamada: Bool = false
    
    let locationManager: CLLocationManager = CLLocationManager()
    
    var beaconRegions: [CLBeaconRegion]!
    
    var activityIndicator: UIActivityIndicatorView!
    
    var origin: String!
    var assistanceReference: DocumentReference!
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Cancel.isHidden = true
        setGradient(viewController: self)
        customizeMainButton(button: mainbutton)
    }
    
    // El usuario pide asistencia
    @IBAction func assistanceCalled(_ sender: Any) {
        if(CheckInternet.Connection()){
            callAssistance()
            mainbutton.isEnabled = false
        } else {
            let alrt = Alert(Title: "Internet connection", Message: "Internet is needed in order to get assistance")
            present(alrt, animated: true, completion: nil)
        }
    }
    
    func callAssistance(){
        locationManager.delegate = self
        
        if(CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        beaconRegions = [CLBeaconRegion(proximityUUID: NSUUID(uuidString: "05F62A3D-F60F-44BC-B36E-2B80FD6C9678")! as UUID, identifier: "lacteos"),
                         CLBeaconRegion(proximityUUID: NSUUID(uuidString: "8F61C017-8D33-4743-B97B-4984B57EBEFD")! as UUID, identifier: "cereal"),
                         CLBeaconRegion(proximityUUID: NSUUID(uuidString: "9D1A0C97-DFB2-4F72-9880-B111313D2437")! as UUID, identifier: "start"),
                         CLBeaconRegion(proximityUUID: NSUUID(uuidString: "E1405AAE-8F28-419B-AB7A-FC263EA9DEE5")! as UUID, identifier: "caja")]
        
        beaconRegions.forEach(locationManager.startRangingBeacons)
        
        mainbutton.setImage(nil, for: .normal)
        showSpinning()
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion){
        print(beacons)
        if(status == POSITION_NOT_FOUND)
        {
            status = FINDING_POSITION
        }
        
        if (status == FINDING_POSITION) {
            calculateCurrentPosition(beacons: beacons)
        }
    }
    
    func calculateCurrentPosition(beacons: [CLBeacon]){
        
        // Geat near beacons
        var categoryBeacons: [String] = [String]()
        
        if(beacons.count > 0){
            for i in 0 ... beacons.count - 1 {
                if (Double(beacons[i].proximity.rawValue) <= 1.0) {
                    categoryBeacons.append(beacons[i].proximityUUID.uuidString)
                }
            }
            
        }
        
        BeaconManager().calculateCurrentPosition(beaconsUuids: categoryBeacons){ response in
            if(response != POSITION_NOT_FOUND){
                self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
                self.origin = response
                
                self.askForAssistance()
            }
            else{
                self.counter = self.counter + 1
                if (self.counter == 10 && !self.asistenciaLlamada){
                    self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
                    print("AQUI")
                    self.asistenciaLlamada = true
                    if let url = URL(string: "tel://\(supermarketTEL)") {
                        UIApplication.shared.openURL(url)
                    }
                    self.assistanceHasEnded()
                }
            }
        }
        
    }
    
    func showSpinning() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        mainbutton.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: mainbutton, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        mainbutton.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: mainbutton, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        mainbutton.addConstraint(yCenterConstraint)
    }
    
    func hideLoading() {
        activityIndicator.stopAnimating()
        mainbutton.imageView?.image = nil
        Cancel.isHidden = true
    }
    
    @IBAction func cancelSelected(_ sender: Any) {
        assistanceHasEnded()
        
        assistanceReference.updateData([
            "status": 3,
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfully written!")
                }
        }
    }
    
    func askForAssistance(){
        
        if(que_paso_amiguito){
            que_paso_amiguito = false
            
            let db = Firestore.firestore()
            
            db.collection("sections").whereField("beaconUuid", isEqualTo: origin).getDocuments{(res, err)in
                if let err = err {
                    print("Error getting documents: \(err)")
                }
                else{
                    print(self.origin)
                    
                    let docRef = db.collection("sections").document((res?.documents[0].documentID)!)

                    self.textShown.text = "Waiting for confirmation"
                    self.Cancel.isHidden = false
                    self.assistanceReference = db.collection("messages").addDocument(data: [
                        "section": docRef,
                        "status": 0
                    ]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        }
                    }
                    
                    self.assistanceReference!.addSnapshotListener { documentSnapshot, error in
                        guard let document = documentSnapshot else {
                            print("Error fetching document: \(error!)")
                            return
                        }
                        if(document.data()!["status"] as! Int == 1){
                            self.hideLoading()
                            self.textShown.text = "Wait for " + ((document.data()!["atiende"]! as? String)!)
                            let url = URL(string: (document.data()!["foto"]! as? String)!)!
                            let data = try! Data(contentsOf: url)
                            self.mainbutton.setImage(UIImage(data: data), for: .normal)
                            
                        } else if(document.data()!["status"] as! Int == 2){
                            self.assistanceHasEnded()
                        }
                    }
                }
            }
        }
    }
    
    func assistanceHasEnded(){
        self.asistenciaLlamada = false
        Cancel.isHidden = true
        counter = 0
        que_paso_amiguito = true
        hideLoading()
        self.textShown.text = "Call assistance"
        mainbutton.setImage(UIImage(named:"phone2"), for: .normal)
        mainbutton.isEnabled = true
    }
    
}
