//
//  SearchingSupermarketViewController.swift
//  EcoApp
//
//  Created by naguilar12 on 11/5/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseFirestore

var supermarket = ""
var supermarketTEL = ""

class SearchingSupermarketViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var searchButton: UIButton!
    var activityIndicator: UIActivityIndicatorView!
    
    let locationManager = CLLocationManager()
    @IBOutlet weak var helpText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGradient(viewController: self)
        customizeMainButton(button: searchButton)
        showSpinning()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            print("NEGADO")
            let alert = UIAlertController(title: "Need Authorization", message: "Authorize this app to use your location in order to locate the supermarket where you are", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplicationOpenSettingsURLString)!
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }))
            self.helpText.text = "Need location permissions"
            activityIndicator.hidesWhenStopped = true
            activityIndicator.stopAnimating()
            self.present(alert, animated: true, completion: nil)
        } else if (status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse) {
            locationManager.startUpdatingLocation()
            showSpinning()
            self.helpText.text = "Looking for supermarket"
            
            print("ACEPDATO")
        } else{
            print("ALA")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.first != nil && CheckInternet.Connection() {
            guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            lookForSupermarket(currentLocation: locValue)
        } else if (!CheckInternet.Connection()){
            let alrt = Alert(Title: "Internet connection", Message: "Internet is needed in order to locate you")
            present(alrt, animated: true, completion: nil)
        }
        if(supermarketTEL != ""){
            self.locationManager.stopUpdatingLocation()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func showSpinning() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        searchButton.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: searchButton, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        searchButton.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: searchButton, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        searchButton.addConstraint(yCenterConstraint)
    }
    
    func lookForSupermarket(currentLocation: CLLocationCoordinate2D){
        let db = Firestore.firestore()
        db.collection("supermarkets").getDocuments{(snapshot, error) in
            
            if error != nil{
                print(error!)
            } else{
                var dictData: [String: Any]
                var documentID: String
                var currentMarketLocation: GeoPoint
                var coordinate₀: CLLocation
                var coordinate₁: CLLocation
                var distanceInMeters: CLLocationDistance
                
                if((snapshot?.documents.count)! - 1 > 0){
                    for i in 0 ... (snapshot?.documents.count)! - 1 {
                        dictData = (snapshot?.documents[i].data())!
                        documentID = (snapshot?.documents[i].documentID)!
                        
                        currentMarketLocation = dictData["location"] as! GeoPoint
                        
                        coordinate₀ = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
                        coordinate₁ = CLLocation(latitude: currentMarketLocation.latitude, longitude: currentMarketLocation.longitude)
                        
                        distanceInMeters = coordinate₀.distance(from: coordinate₁) // result is in meters
                        
                        if(distanceInMeters <= 1000){
                            
                            supermarket = documentID
                            supermarketTEL = dictData["phone"] as! String
                            self.performSegue(withIdentifier: "supermarketFound", sender: self)
                        }
                    }
                }
            }
        }
    }
    
}
