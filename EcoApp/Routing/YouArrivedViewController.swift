//
//  YouArrivedViewController.swift
//  EcoApp
//
//  Created by naguilar12 on 10/7/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit

class YouArrivedViewController: UIViewController {
    
    @IBOutlet weak var flagButton: UIButton!
    @IBOutlet weak var beMyEyesButton: UIButton!
    @IBOutlet weak var continueCloseButton: UIButton!
    
    
    var routeHasEnded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(!routeHasEnded){
            continueCloseButton.setTitle("Continue route", for: .normal)
        }
        
        setGradient(viewController: self)
        customizeMainButton(button: flagButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func continueCloseButtonSelected(_ sender: Any) {
        if(routeHasEnded){
            performSegue(withIdentifier: "backHome", sender: self)
        }
        else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    
}
