//
//  RouteViewController.swift
//  EcoApp
//
//  Created by naguilar12 on 10/5/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth

class RouteViewController: UIViewController, CLLocationManagerDelegate, CBCentralManagerDelegate {
    
    var status: String!
    var counter = 0
    var yaEmpezo: Bool = false
    var destinationOrientation = 360.0
    
    var voiceTimer: Timer!
    var lastheading = 0.0
    let speechSynthesizer = AVSpeechSynthesizer()
    var shouldTalk = true
    
    var manager:CBCentralManager!
    
    var BTS: Bool!
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            break
        case .poweredOff:
            self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
            performSegue(withIdentifier: "cancelRoute", sender: self)
            let alrt = Alert(Title: "Bluetooth", Message: "Bluetooth was disabled, please enabled it again")
            present(alrt, animated: true, completion: nil)
            break
        case .unknown:
            break
        case .resetting:
            break
        case .unsupported:
            break
        case .unauthorized:
            break
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        manager.stopScan()
        self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
        shouldTalk = false
        
    }
    
    var route: [[NSDictionary]] = [[NSDictionary]]()
    var origin: String!
    var destinations: [String] = [String]()
    
    var beaconRegions: [CLBeaconRegion]!
    let locationManager: CLLocationManager = {
        $0.requestWhenInUseAuthorization()
        $0.startUpdatingHeading()
        return $0
    }(CLLocationManager())
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        UIView.animate(withDuration: 0.5) {
            if(self.status != POSITION_NOT_FOUND){
                self.lastheading = newHeading.trueHeading
                let angle = ((self.destinationOrientation - newHeading.trueHeading) * .pi) / 180 // convert from degrees to radians
                //self.imageView.transform = CGAffineTransform(rotationAngle: angle) // rotate the picture
                self.directionButton.transform = CGAffineTransform(rotationAngle: CGFloat(angle)) // rotate the picture
            }
        }
    }
    
    @objc func voiceMoving(){
        
        if(shouldTalk){
            print("DICE QUE SE MUEVA A UNA DIRECCION")
            
            let directionToTalk = (self.destinationOrientation - lastheading)
            print(directionToTalk)
            
            if(directionToTalk < 45.0 && directionToTalk > -45.0){
                print("Says GO FORWARDS")
                let say = "Go Forwards"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
            }
            else if(directionToTalk > 45.0 && directionToTalk < 135){
                print("Says GO RIGHT")
                let say = "Go Right"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
            }
            else if(directionToTalk < -45.0 && directionToTalk > -135.0){
                print("Says GO LEFT")
                let say = "Go Left"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
            }
            else{
                print("Says GO BACKWARDS")
                let say = "Go Backwards"
                let speechUtterance = AVSpeechUtterance(string: say)
                //This line reproduces sound and executes speechSynthesizer method
                speechSynthesizer.speak(speechUtterance)
            }
        }
    }
    
    @IBOutlet weak var testLabel: UILabel!
    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        shouldTalk = true
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setGradient(viewController: self)
        customizeDirectionButton()
        
        manager = CBCentralManager()
        manager.delegate = self
        
        status = FINDING_POSITION
        yaEmpezo = false
        
        locationManager.delegate = self
        if(CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        testLabel.text = "FINDING YOUR CURRENT POSITION"
        
        
        beaconRegions = [CLBeaconRegion(proximityUUID: NSUUID(uuidString: "05F62A3D-F60F-44BC-B36E-2B80FD6C9678")! as UUID, identifier: "lacteos"),
                         CLBeaconRegion(proximityUUID: NSUUID(uuidString: "8F61C017-8D33-4743-B97B-4984B57EBEFD")! as UUID, identifier: "cereal"),
                         CLBeaconRegion(proximityUUID: NSUUID(uuidString: "9D1A0C97-DFB2-4F72-9880-B111313D2437")! as UUID, identifier: "start"),
                         CLBeaconRegion(proximityUUID: NSUUID(uuidString: "E1405AAE-8F28-419B-AB7A-FC263EA9DEE5")! as UUID, identifier: "caja")]
        
        beaconRegions.forEach(locationManager.startRangingBeacons)
        
        voiceTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(voiceMoving), userInfo: nil, repeats: true)

    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion){
        print(beacons)
        if(status == POSITION_NOT_FOUND)
        {
            status = FINDING_POSITION
        }
        
        if (status == FINDING_POSITION && !yaEmpezo) {
            calculateCurrentPosition(beacons: beacons)
        } else if (status == ON_ROUTE) {
            handleRouting(beacons: beacons)
        }
    }
    
    func calculateCurrentPosition(beacons: [CLBeacon]){
        
        // Geat near beacons
        var categoryBeacons: [String] = [String]()
        if(beacons.count > 0){
            for i in 0 ... beacons.count - 1 {
                if (Double(beacons[i].proximity.rawValue) <= 1.0) {
                    categoryBeacons.append(beacons[i].proximityUUID.uuidString)
                }
            }
            
        }
        
        if(!CheckInternet.Connection()){
            self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
            performSegue(withIdentifier: "cancelRoute", sender: self)
            let alrt = Alert(Title: "Internet", Message: "Please connect to internet in order to receive our services")
            present(alrt, animated: true, completion: nil)
        }
        
        BeaconManager().calculateCurrentPosition(beaconsUuids: categoryBeacons){ response in
            if(response != POSITION_NOT_FOUND){
                self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
                self.yaEmpezo = true
                self.origin = response
                self.testLabel.text = "CALCULATING ROUTE"
                self.updateRouting()
                
            }
            else{
                self.counter = self.counter + 1
                if(self.counter == 5){
                    self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
                    
                    self.counter = 0
                    self.testLabel.text = "Could not find your current position"
                    self.hideLoading()
                    self.setupCurrentRouteStep(direction: "warning")
                    self.status = POSITION_NOT_FOUND
                }
            }
        }
    }
    
    
    func handleRouting(beacons: [CLBeacon]){
        var nextStep: NSDictionary = route[route.count-1][route[route.count-1].count-1]
        destinationOrientation = nextStep["bearing"] as! Double
        
        if(beacons.count > 0){
            for i in 0 ... beacons.count-1 {
                if (Double(beacons[i].proximity.rawValue) <= 1.0) {
                    
                    if (nextStep["to"] as! String).caseInsensitiveCompare(beacons[i].proximityUUID.uuidString) == ComparisonResult.orderedSame {
                        
                        route[route.count-1].remove(at: route[route.count-1].count - 1)
                        
                        if route[route.count-1].isEmpty{
                            route.remove(at: route.count - 1)
                            destinations = destinations.filter {$0 != nextStep["to"] as! String}
                            if !route.isEmpty{
                                youArrived()
                            }
                        }
                        if(!route.isEmpty){
                            nextStep = route[route.count-1][route[route.count-1].count-1]
                            self.testLabel.text = "Move " + (nextStep["direction"] as! String) + ", " + String(nextStep["distance"] as! Int) + " " + (nextStep["unit"] as! String)
                        }
                        else{
                            youArrived()
                        }
                        
                    }
                }
            }
        }
    }
    
    /*
     func updateRouting(){
     BeaconManager().getRoutingTable(origin: origin, destination: destination){beaconArrayRoute in
     self.route.removeAll()
     for b in beaconArrayRoute {
     self.route.append(b as! NSDictionary)
     }
     self.route = self.route.reversed()
     self.startRoute()
     
     print("Esta es mi ruta")
     self.testLabel.text = "Move " + (self.route[self.route.count-1]["direction"] as! String) + ", " + String(self.route[self.route.count-1]["distance"] as! Int) + " " + (self.route[self.route.count-1]["unit"] as! String)
     
     self.hideLoading()
     self.setupCurrentRouteStep(direction: self.route[self.route.count-1]["direction"] as! String)
     
     
     print(self.route)
     }
     }
     */
    func updateRouting(){
        if destinations.contains(origin){
            destinations = destinations.filter {$0 != origin}
            youArrived()
        }
        
        if(!CheckInternet.Connection()){
            performSegue(withIdentifier: "cancelRoute", sender: self)
            let alrt = Alert(Title: "Internet", Message: "Please connect to internet in order to receive our services")
            present(alrt, animated: true, completion: nil)
        }
        
        print("DESTINATIONS")
        print(destinations)
        if(destinations.count > 0){
            BeaconManager().getMultipleRoutingTable(origin: origin, sections: destinations, supermarket: supermarket) {response in
                
                self.route = [[NSDictionary]]()
                
                var currentRoute:[NSDictionary] = [NSDictionary]()
                for index in 0...response.count-1{
                    currentRoute = response[response.count-1-index] as! [NSDictionary]
                    currentRoute.reverse()
                    self.route.append(currentRoute)
                }
                print(self.origin)
                print(self.destinations)
                print(self.route)
                self.startRoute()
                
                let firstStep = self.route[self.route.count-1][self.route[self.route.count-1].count-1]
                self.testLabel.text = "Move " + (firstStep["direction"] as! String) + ", " + String(firstStep["distance"] as! Int) + " " + (firstStep["unit"] as! String)
                
                self.hideLoading()
                self.setupCurrentRouteStep(direction: "Forward")}
        }
    }
    
    
    
    func startRoute(){
        testLabel.text = "Esta es mi ruta"
        status = ON_ROUTE
        beaconRegions.forEach(locationManager.startRangingBeacons)
    }
    
    
    @IBAction func touchUpInsideDirectionButton(_ sender: UIButton) {
        if(status == POSITION_NOT_FOUND){
            showSpinning()
            status = FINDING_POSITION
            beaconRegions.forEach(locationManager.startRangingBeacons)
            self.directionButton.setImage(UIImage(), for: .normal)
            
        }
    }
    
    @IBAction func cancelButtonSelected(_ sender: Any) {
        self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
        performSegue(withIdentifier: "cancelRoute", sender: self)
    }
    
    @IBAction func addNewStopButtonSelected(_ sender: Any) {
        self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
        performSegue(withIdentifier: "addNewStop", sender: self)
    }
    
    
    func youArrived(){
        if(route.isEmpty && (status != FINDING_POSITION || destinations.count == 0)){
            self.beaconRegions.forEach(self.locationManager.stopRangingBeacons)
            yaEmpezo = false
            status = FINDING_POSITION
        }
        
        let say = "You have arrived"
        let speechUtterance = AVSpeechUtterance(string: say)
        //This line reproduces sound and executes speechSynthesizer method
        speechSynthesizer.speak(speechUtterance)
        
        performSegue(withIdentifier: "youHaveArrived", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "youHaveArrived"){
            let destVC: YouArrivedViewController = segue.destination as! YouArrivedViewController
            destVC.routeHasEnded = !yaEmpezo
        } else if (segue.identifier == "addNewStop"){
            let destVC: CategoriesViewController = segue.destination as! CategoriesViewController
            destVC.destinations = destinations
        }
    }
    
    
    func setupCurrentRouteStep(direction: String) {
        switch direction {
            
        case "Forward":
            self.directionButton.setImage(UIImage(named: "north.png"), for: .normal)
        case "Left":
            self.directionButton.setImage(UIImage(named: "west.png"), for: .normal)
        case "Right":
            self.directionButton.setImage(UIImage(named: "east.png"), for: .normal)
        case "Backwards":
            self.directionButton.setImage(UIImage(named: "south.png"), for: .normal)
        case "warning":
            self.directionButton.setImage(UIImage(named: "warning.png"), for: .normal)
        default:
            break
        }
    }
    
    func customizeDirectionButton() {
        let color = (UIColor(red: (5/255), green: (151/255), blue: (242/255), alpha: 0.8))
        directionButton.layer.masksToBounds = true
        directionButton.layer.cornerRadius = 75
        directionButton.layer.borderWidth = 5
        directionButton.layer.borderColor = color.cgColor
        
        showSpinning()
    }
    
    func showSpinning() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        directionButton.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: directionButton, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        directionButton.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: directionButton, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        directionButton.addConstraint(yCenterConstraint)
    }
    
    func hideLoading() {
        activityIndicator.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
