//
//  ListsViewController.swift
//  EcoApp
//
//  Created by naguilar12 on 12/9/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit

class ListsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    // Listas personalizadas del usuario
    var lists = [String]()
    // Listas que cumplen el filtro de texto
    var filteredLists = [String]()
    
    // Ids de los beacons que contiene la lista seleccionada
    var destinations = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    
    // Controllers necesarios para filtrar por texto
    var searchController : UISearchController!
    var resultsController = UITableViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CustomCell.self, forCellReuseIdentifier: "prototype")
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Inicializar la barra de busqueda y table view auxiliar
        self.creatingSearhBar()
        self.tableSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        loadLists()
        self.tableView.reloadData()

    }
    
    // Metodo para crear la Search Bar
    func creatingSearhBar() {
        self.searchController = UISearchController(searchResultsController: self.resultsController)
        self.tableView.tableHeaderView = self.searchController.searchBar
        self.searchController.searchResultsUpdater = self as UISearchResultsUpdating
    }
    
    // Ajustar la table view de resultados filtrados
    func tableSettings() {
        self.resultsController.tableView.dataSource = self
        self.resultsController.tableView.delegate = self
        
        self.resultsController.tableView.allowsMultipleSelection = true
        self.resultsController.tableView.allowsMultipleSelectionDuringEditing = true
        
        self.resultsController.tableView.register(CustomCell.self, forCellReuseIdentifier: "prototype")
        self.resultsController.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func loadLists(){
        lists = UserDefaults.standard.stringArray(forKey: "Lists") ?? [String]()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return lists.count
        }
        else {
            return filteredLists.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "prototype")
        
        if tableView == self.tableView {
            cell?.textLabel?.text = String(lists[indexPath.row].split(separator: ":")[0])
            
            cell!.layoutSubviews()
        }else{
            cell?.textLabel?.text = String(filteredLists[indexPath.row].split(separator: ":")[0])
            cell!.layoutSubviews()
        }
        
        return cell!
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filteredLists = self.lists.filter { (list: String) -> Bool in
            if list.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
                return true
            } else{
                return false
            }
        }
        
        self.resultsController.tableView.reloadData()
    }
    
    @IBAction func addList(_ sender: Any) {
        performSegue(withIdentifier: "addList", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "listSelected"){
            let destVC: RouteViewController = segue.destination as! RouteViewController
            destVC.destinations = self.destinations
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let listName = cell?.textLabel?.text
    
        for list in lists{
            let name = String(list.split(separator: ":")[0])
            if(name == listName){
                destinations = String(list.split(separator: ":")[1]).components(separatedBy: ";")
            }
        }
        performSegue(withIdentifier: "listSelected", sender: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
