//
//  ViewSetup.swift
//  EcoApp
//
//  Created by naguilar12 on 11/3/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

func setGradient(viewController: UIViewController){
    let topColor = UIColor(red: (0/255), green: (187/255), blue: (255/255), alpha: 1)
    let centerColor = UIColor(red: (5/255), green: (131/255), blue: (242/255), alpha: 1)
    let bottomColor = UIColor(red: (5/255), green: (131/255), blue: (242/255), alpha: 1)
    
    let gradientColors: [CGColor] = [topColor.cgColor, centerColor.cgColor,bottomColor.cgColor]
    let gradientLocations: [Float] = [0.0, 0.5, 1.0]
    
    let gradientLayer: CAGradientLayer = CAGradientLayer()
    gradientLayer.colors = gradientColors
    gradientLayer.locations = gradientLocations as [NSNumber]
    
    gradientLayer.frame = viewController.view.bounds
    viewController.view.layer.insertSublayer(gradientLayer, at: 0)
}

func customizeMainButton(button: UIButton) {
    let color = (UIColor(red: (5/255), green: (151/255), blue: (242/255), alpha: 0.8))
    button.layer.masksToBounds = true
    button.layer.cornerRadius = 75
    button.layer.borderWidth = 5
    button.layer.borderColor = color.cgColor
}
