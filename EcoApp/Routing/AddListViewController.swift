//
//  CategoriesViewController.swift
//  EcoApp
//
//  Created by naguilar12 on 10/4/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit
import CoreBluetooth

class AddListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    // Informacion sobre la cual se hace la lista
    var categories = [CellData]()
    // Lista de categorias filtradas por texto
    var filteredCategories = [CellData]()
    // Destinos seleccionados parcialmente
    var destinations: [String] = [String]()
    
    // Controllers necesarios para filtrar por texto
    var searchController : UISearchController!
    var resultsController = UITableViewController()
    
    
    // Table View con todas las categorias
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargar categorias de firebase
        loadCategories()
        
        // Ajustes para el table view principal
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.register(CustomCell.self, forCellReuseIdentifier: "custom")
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Inicializar la barra de busqueda y table view auxiliar
        self.creatingSearhBar()
        self.tableSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Ocultar el nav bar
        self.navigationController?.isNavigationBarHidden = false
        // Ocultar el tab bar
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ////////////////////////////////////////////////////////////////////
    //Carga de categorias///////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    
    func loadCategories(){
        DispatchQueue.global(qos: .background).async {
            CacheHelper.getCategories(){response in
                self.categories = response
                DispatchQueue.main.async{
                    self.tableView.reloadData()
                }
                
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////
    //Search Bar////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    
    // Metodo para crear la Search Bar
    func creatingSearhBar() {
        self.searchController = UISearchController(searchResultsController: self.resultsController)
        self.tableView.tableHeaderView = self.searchController.searchBar
        self.searchController.searchResultsUpdater = self as UISearchResultsUpdating
    }
    
    // Ajustar la table view de resultados filtrados
    func tableSettings() {
        self.resultsController.tableView.dataSource = self
        self.resultsController.tableView.delegate = self
        
        self.resultsController.tableView.allowsMultipleSelection = true
        self.resultsController.tableView.allowsMultipleSelectionDuringEditing = true
        
        self.resultsController.tableView.register(CustomCell.self, forCellReuseIdentifier: "custom")
        self.resultsController.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    // Criterio de filtro
    func updateSearchResults(for searchController: UISearchController) {
        self.filteredCategories = self.categories.filter { (category: CellData) -> Bool in
            if category.customText!.lowercased().contains(self.searchController.searchBar.text!.lowercased()){
                return true
            } else{
                return false
            }
        }
        
        self.resultsController.tableView.reloadData()
    }
    
    ////////////////////////////////////////////////////////////////////
    //Table views///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    
    // # de filas en la tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return categories.count
        }
        else {
            return filteredCategories.count
        }
    }
    
    // Inicializar las celdas de la tabla
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "custom") as! CustomCell
        print("cellForRowAt")
        
        if tableView == self.tableView {
            if destinations.contains(categories[indexPath.row].beaconUuid!){
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            
            cell.customImage = categories[indexPath.row].customImage
            cell.customText = categories[indexPath.row].customText
            cell.layoutSubviews()
        }else{
            if destinations.contains(filteredCategories[indexPath.row].beaconUuid!){
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            else{
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            cell.customImage = filteredCategories[indexPath.row].customImage
            cell.customText = filteredCategories[indexPath.row].customText
            cell.layoutSubviews()
        }
        
        return cell
        
    }
    
    // Manejo de la seleccion de una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        if(cell!.accessoryType == UITableViewCellAccessoryType.checkmark){
            print("Deselect")
            if tableView == self.tableView {
                destinations = destinations.filter {$0 != categories[indexPath.row].beaconUuid}
            }
            else{
                destinations = destinations.filter {$0 != filteredCategories[indexPath.row].beaconUuid}
            }
            print(destinations.count)
            self.tableView.reloadData()
            self.resultsController.tableView.reloadData()
        }
        else if(cell!.accessoryType == UITableViewCellAccessoryType.none){
            print("Select")
            if tableView == self.tableView {
                destinations.append(categories[indexPath.row].beaconUuid!)
            }
            else{
                destinations.append(filteredCategories[indexPath.row].beaconUuid!)
            }
            print(destinations.count)
            self.tableView.reloadData()
            self.resultsController.tableView.reloadData()
        }
    }
    
    @IBAction func cancelSelected(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneSelected(_ sender: Any) {
        if(destinations.count > 0){
            var beaconIds = destinations[0]
            
            if(destinations.count > 1){
                for i in 1 ... destinations.count - 1{
                    beaconIds = beaconIds + ";" + destinations[i]
                }
            }
            
            var name = ""
            
            //1. Create the alert controller.
            let alert = UIAlertController(title: "List title", message: "Enter a title", preferredStyle: .alert)
            
            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                textField.text = "Title"
            }
            
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                name = alert!.textFields![0].text! // Force unwrapping because we know it exists.
                
                var actual = UserDefaults.standard.stringArray(forKey: "Lists") ?? [String]()
                actual.append(name + ":" + beaconIds)
                UserDefaults.standard.set(actual, forKey: "Lists")
                
                print(UserDefaults.standard.object(forKey: "Lists") as! [String])
                self.navigationController?.popViewController(animated: true)
            }))
            
            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
            
        }
        else{
            let alrt = Alert(Title: "New List", Message: "Select at least one category")
            present(alrt, animated: true, completion: nil)
        }
    }
    
    
    
}
