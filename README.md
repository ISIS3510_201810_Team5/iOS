## Dependencies:

- macOS
- Cocoapods

Can be installed typing `sudo gem install cocoapods` on your terminal

## Steps to run in xcode:

1. download or clone with: `git clone https://gitlab.com/ISIS3510_201810_Team5/iOS.git`

2. On your terminal, on the main project folder (called iOS) type `pod install`

3. Start Xcode and open EcoApp.xcworkspace

4. Select an iOS Device simulator and run build or select a real connected device (via USB) and build.


## Steps setup Firebase:

1. Create a Firebase project at: https://console.firebase.google.com
2. Add a new iOS app to that project and follow the instructions (only copying the GoogleService-info.plist to the main project folder should be necessary)
3. 
