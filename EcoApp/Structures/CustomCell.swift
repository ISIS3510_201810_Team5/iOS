//
//  CustomCell.swift
//  EcoApp
//
//  Created by naguilar12 on 10/4/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell{
    
    var customText: String?
    var customImage: UIImage?
    var beaconUuid: String?
    
    var customTextView : UILabel = {
        var textView = UILabel()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    var customImageView : UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(customImageView)
        self.addSubview(customTextView)
        
        customImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        customImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        customImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        customImageView.widthAnchor.constraint(equalTo: self.heightAnchor).isActive = true

        customImageView.heightAnchor.constraint(equalToConstant: 75).isActive = true

        
        customTextView.leftAnchor.constraint(equalTo: self.customImageView.rightAnchor).isActive = true
        customTextView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        customTextView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        customTextView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true

        customTextView.isUserInteractionEnabled = false
        
        customTextView.font = UIFont(name: "ArialMT", size: 18)
        
        customImageView.layer.masksToBounds = true
        customImageView.layer.cornerRadius = 75/2

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let customText = customText {
            customTextView.text = "    " + customText
        }
        
        if let customImage = customImage {
            customImageView.image = customImage
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
