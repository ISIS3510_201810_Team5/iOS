//
//  CameraViewController.swift
//  EcoApp
//
//  Created by naguilar12 on 10/4/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    private lazy var captureSession: AVCaptureSession = {
        let captureSession = AVCaptureSession()
        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back),
            let input = try? AVCaptureDeviceInput(device: captureDevice) else {
                return captureSession
        }
        captureSession.addInput(input)
        let output = AVCaptureVideoDataOutput()
        output.setSampleBufferDelegate(self, queue: DispatchQueue(label: "SampleBuffer"))
        captureSession.addOutput(output)
        return captureSession
    }()
    
    let speechSynthesizer = AVSpeechSynthesizer()
    lazy var vision = Vision.vision()
    var textDetector: VisionTextRecognizer?
    private var textsToRead = [String]()
    var products = [[String: Any]]()
    var dataRetrieved:Bool = false
    var shouldRead = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Database
        loadData()
        //DispatchQueue.main.asyncAfter(deadline: .now() + 4) { // change 2 to desired number of seconds
        // Your code with delay
        //    print(self.products[2]["name"] as! String)
        //}
        // Speech
        speechSynthesizer.delegate = self
        view.layer.addSublayer(previewLayer)
        try? setPlaybackAudioSession()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        shouldRead = false
        textsToRead.removeAll()
        captureSession.stopRunning()
        textDetector = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        shouldRead = true
        previewLayer.frame = view.frame
        checkCameraAccess()
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    func loadData() {
        if CheckInternet.Connection(){
            let dbProducts = Firestore.firestore().collection("products").getDocuments { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        self.products.append(document.data())
                        print(document.data()["name"] as! String)
                    }
                    self.dataRetrieved = true
                }
            }
        }
        else {
            let alrt = Alert(Title: "Internet connection", Message: "Internet is needed to identify ")
            present(alrt, animated: true, completion: nil)
        }
    }
    
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: captureSession.startRunning()
        default: requestCameraAccess()
        }
    }
    
    func requestCameraAccess() {
        AVCaptureDevice.requestAccess(for: .video) { _ in
            self.checkCameraAccess()
        }
    }
    
    func setPlaybackAudioSession() throws {
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryPlayback)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
    }
    
    func read(_ texts: [String]) {
        //appends all texts to variable
        if (dataRetrieved) {
            
            if let prod  = findProduct(wordsSaid: texts) {
                textsToRead.append("hello")
                
                //product
                textsToRead.append("the product's name is")
                textsToRead.append(prod["name"] as! String)
                
                //price
                textsToRead.append("it costs")
                let price = prod["price"] as! [String: Any]
                let priceInt = price["units"] as! Int
                let iso = price["iso"] as! String
                
                
                if (iso == "COP") {
                    textsToRead.append("\(priceInt) Colombian Peso")
                }
                else if (iso == "USD") {
                    textsToRead.append("\(priceInt) US Dolar")
                }
                
                
                //description
                textsToRead.append("the product description is")
                textsToRead.append(prod["description"] as! String)
                
                read(textsToRead.removeFirst())
            }
            else {
                textsToRead.append("product not found try again")
                read(textsToRead.removeFirst())
            }
        }
        else {
            textsToRead.append(contentsOf: texts)
            read(textsToRead.removeFirst())
            
        }
        
    }
    
    func findProduct(wordsSaid: [String]) -> [String: Any]? {
        
        // products tipo: [[String: Any]]
        
        var name: String = ""
        for word in wordsSaid {
            for product in products {
                name = product["name"] as! String
                if name.lowercased() == word.lowercased() || word.lowercased().contains(name.lowercased()){
                    return product
                }
            }
        }
        return nil
    }
    
    func read(_ text: String) {
        let speechUtterance = AVSpeechUtterance(string: text)
        /* language detection, English is default
         let dominantLanguage = NSLinguisticTagger.dominantLanguage(for: text)
         speechUtterance.voice = AVSpeechSynthesisVoice(language: dominantLanguage)
         */
        
        //This line reproduces sound and executes speechSynthesizer method
        speechSynthesizer.speak(speechUtterance)
    }
    
}

extension CameraViewController: AVSpeechSynthesizerDelegate {
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        if(shouldRead) {
            if textsToRead.isEmpty {
                textDetector = nil
                captureSession.startRunning()
            } else {
                read(textsToRead.removeFirst())
            }
        }
    }
    
}

extension CameraViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if textDetector == nil {
            let metadata = VisionImageMetadata()
            metadata.orientation = .rightTop
            let visionImage = VisionImage(buffer: sampleBuffer)
            visionImage.metadata = metadata
            textDetector = vision.onDeviceTextRecognizer()
            textDetector?.process(visionImage){ result, error in
                guard error == nil, let result = result, !result.text.isEmpty else {
                    self.textDetector = nil
                    return
                }
                self.captureSession.stopRunning()
                var features = result.text.components(separatedBy: " ")
                self.read(features)
            }
        }
    }
}
