//
//  File.swift
//  EcoApp
//
//  Created by Alejandro Echeverri on 11/5/18.
//  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
//


func Alert (Title: String, Message: String) -> UIAlertController{
    
    let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
    return alert
}
//present(alert, animated: true, completion: nil)

// Delay method
//DispatchQueue.main.asyncAfter(deadline: .now() + 4) { // change 2 to desired number of seconds
// Your code with delay
//    print(self.products[2]["name"] as! String)
//}
