 //
 //  CacheHelper.swift
 //  EcoApp
 //
 //  Created by naguilar12 on 11/4/18.
 //  Copyright © 2018 Nicolas_Aguilar. All rights reserved.
 //
 
 import Foundation
 import UIKit
 import Alamofire
 import FirebaseFirestore
 
 
 class CacheHelper {
    static let CATEGORIES_URL = "https://us-central1-test-c4c72.cloudfunctions.net/categories"
    static let cache = NSCache <NSString, AnyObject>()
    
    static func downloadCategories(completionHandler:@escaping ([CellData]) -> Void){
        var categories = [CellData]()      
        let db = Firestore.firestore()
        
        db.collection("sections").getDocuments{(snapshot, error) in
            if error != nil{
                print(error!)
            } else{
                var dictData: [String: Any]
                var docReference: DocumentReference
                
                var url: URL
                var data: Data
                var categoryName: String
                
                for i in 0 ... (snapshot?.documents.count)! - 1 {
                    dictData = (snapshot?.documents[i].data())!
                    docReference = dictData["supermarket"] as! DocumentReference
                    
                    if(docReference.documentID == supermarket){
                        url = URL(string: (dictData["imageUrl"]! as? String)!)!
                        data = try! Data(contentsOf: url)
                        categoryName = (dictData["name"]! as? String)!
                            
                        categoryName = categoryName.prefix(1).uppercased() + categoryName.dropFirst()
                        categories.append(CellData.init(customImage: UIImage(data: data), customText: categoryName, beaconUuid: dictData["beaconUuid"]! as? String))
                    }
                }
                cache.setObject(categories as AnyObject, forKey: "supermarkets/"+supermarket as NSString)
                completionHandler(categories)
            }
        }
    }
    
    static func getCategories(completionHandler:@escaping ([CellData]) -> Void){
        if let categories = cache.object(forKey: "supermarkets/"+supermarket as NSString){
            completionHandler(categories as! [CellData])
        } else{
            downloadCategories(completionHandler: completionHandler)
        }
    }
    
    static func categoriesOnCache() -> Bool{
        let categories = cache.object(forKey: "supermarkets/"+supermarket as NSString)
        return categories != nil
    }
 }
